import logging
import pkg_resources
from pathlib import Path
from math import inf
from shutil import copyfileobj
from io import StringIO
from typing import Union, List, Tuple, Any
from functools import partial as bind
from robogcode.gcode import GCodeParser
from robogcode.output import RobotOutput
from robogcode.processor import ProcessorDeps
from robogcode.misc import Dependency, DependencyList


LOG = logging.getLogger(__name__)


def _append(lst: list, data: Union[List[Any], Any]) -> None:
    if isinstance(data, list):
        lst.extend(data)
    else:
        lst.append(data)


class GCodeTranspilerException(Exception):
    pass


class GCodeTranspiler:
    """
    Transform gcode into another format
    """

    def __init__(self, output: RobotOutput, processors: ProcessorDeps=None, extras: bool=True):
        self._output = output
        self._extras = extras
        self._procs = None
        self._eol = self._output.eol()
        self._limit = self._output.linelimit()
        self._encoding = self._output.encoding()
        self._gcode = ""

        if self._limit is None or self._limit < 1:
            self._limit = inf

        procs = []

        if processors is not None:
            _append(procs, processors)

        if hasattr(self._output, "__deps__"):
            _append(procs, self._output.__deps__)

        if len(procs) > 0:
            self._procs = self._handle_procs(procs)

    def _process_lines(self, code: Union[List[str], str]) -> Tuple[int, str]:
        if code is None or len(code) == 0:
            return (0, "")
        elif isinstance(code, list):
            filtered = [line for line in code if len(line) > 0]
            return (len(filtered), self._eol.join(filtered))

        return (1, code)

    def _handle_procs(self, processors: ProcessorDeps) -> List:
        get_prio = lambda x: getattr(x, "__prio__", Dependency.PRIORITY_DEFAULT)
        deplist = DependencyList()
        procs = processors.copy()

        while len(procs) > 0:
            proc = procs.pop()

            if hasattr(proc, "__deps__"):
                deps = proc.__deps__

                if not isinstance(deps, list):
                    procs.append(deps)
                    deplist.append(Dependency(deps, get_prio(deps)))
                else:
                    for dep in deps:
                        procs.append(dep)
                        deplist.append(Dependency(dep, get_prio(dep)))

            deplist.append(Dependency(proc, get_prio(proc)))

        if len(deplist) > 0:
            deps = deplist.dependencies(lambda d: d())
            enabled = ", ".join([p.__class__.__name__ for p in deps])
            LOG.info("Enabled processors: %s", enabled)
            return deps

        return None

    def _hook(self, gcode: List[Tuple[str, dict]], cmd: str, params: dict) -> None:
        if self._procs is None:
            gcode.append((cmd, params))
            return

        last_cmd = [(cmd, params)]
        next_cmd = []

        for proc in self._procs:
            for c, p in last_cmd:
                ret = proc(c, p)

                if isinstance(ret, bool):
                    if not ret:
                        continue
                    else:
                        next_cmd.append((c, p))
                else:
                    _append(next_cmd, ret)

            last_cmd = next_cmd.copy()

            if len(next_cmd) == 0:
                break

            next_cmd.clear()

        if len(last_cmd) > 0:
            gcode.extend(last_cmd)

    def _open_buffer(self) -> StringIO:
        return StringIO(newline="")

    def _write_buffer(self, buf: StringIO, directory: str, fileno: int) -> None:
        file = Path(directory) / self._output.filename(fileno)

        LOG.debug("Writing buffer to file: %s, encoding=%s", file.absolute(), self._encoding)

        buf.seek(0)

        with file.open(mode="w", encoding=self._encoding, newline="") as out:
            copyfileobj(buf, out)

        buf.close()

    def _write_pre(self, buf: StringIO, fileno: int) -> None:
        _, pre = self._process_lines(self._output.pre(fileno))

        if len(pre) > 0:
            LOG.debug("Writing pre for fileno: %s", fileno)
            buf.write(pre)
            buf.write(self._eol)

    def _write_post(self, buf: StringIO, fileno: int, last: bool) -> None:
        _, post = self._process_lines(self._output.post(fileno, last))

        if len(post) > 0:
            LOG.debug("Writing post for fileno: %s", fileno)
            buf.write(post)
            buf.write(self._eol)

    def _extra_files(self, directory: str) -> None:
        files = self._output.files()

        if files is None or len(files) == 0:
            return

        outdir = Path(directory)
        LOG.info("Copying extra files to directory: %s", directory)

        for f in files:
            dst = outdir / Path(f).name
            src = pkg_resources.resource_stream("robogcode", "data/" + f)

            if not dst.exists():
                LOG.debug("%s -> %s", f, dst)

                with dst.open(mode="wb") as handle:
                    copyfileobj(src, handle)

    def read(self, filepath: str) -> None:
        """Read gcode from path or file-like object"""

        if isinstance(filepath, str):
            LOG.info("Reading gcode from file: %s", filepath)

            with open(filepath, "r") as handle:
                self._gcode = handle.read()
        elif hasattr(filepath, "read"):
            LOG.info("Reading gcode from stream")
            self._gcode = filepath.read()

    def write(self, directory: str) -> None:
        """Write resulting code to directory"""

        fileno = 0
        lines = 0
        buf = None
        gcode = []
        default = lambda *args, **kwargs: None
        gcparser = GCodeParser()
        gcparser.parse(self._gcode, bind(self._hook, gcode))
        gcode_lines = len(gcode)

        if gcode_lines == 0:
            raise GCodeTranspilerException("No gcode")

        LOG.info("Writing output to directory: %s", directory)

        try:
            buf = self._open_buffer()
            self._write_pre(buf, fileno)

            for cmd, params in gcode:
                gcode_lines -= 1
                method_name = cmd.replace(".", "_")
                result = getattr(self._output, method_name, default)(params)
                ln, code = self._process_lines(result)
                lines += ln

                if len(code) > 0:
                    buf.write(code)
                    buf.write(self._eol)

                if lines >= self._limit:
                    lines = 0
                    self._write_post(buf, fileno, gcode_lines == 0)
                    self._write_buffer(buf, directory, fileno)

                    if gcode_lines > 0:
                        fileno += 1
                        buf = self._open_buffer()
                        self._write_pre(buf, fileno)

            if not buf.closed:
                self._write_post(buf, fileno, True)
                self._write_buffer(buf, directory, fileno)

            LOG.info("Wrote %d file(s) successfully", fileno + 1)
        except (OSError, IOError) as e:
            raise e
        else:
            if self._extras:
                self._extra_files(directory)
