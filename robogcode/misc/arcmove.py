import math
from typing import List
from robogcode.math import Point, Vector


class ArcMove:
    """
    Helper class for calculating a list of points from G2/G3 moves
    """

    @staticmethod
    def _calculate(current, end, center, num_points, plane, direction) -> List[Point]:
        points = []
        p1, p2 = plane
        vec = Vector.from_points(center, end)
        radius = vec.length
        sc_angle = math.atan2(current[p2] - center[p2], current[p1] - center[p1])
        ec_angle = math.atan2(end[p2] - center[p2], end[p1] - center[p1])
        is_equal = current.equal(end)

        if sc_angle < 0:
            sc_angle += math.tau

        if ec_angle < 0:
            ec_angle += math.tau

        if is_equal:
            arc1 = arc2 = math.tau / (num_points - 1)
        else:
            angle = abs(sc_angle - ec_angle)
            arc1 = angle / (num_points - 1)
            arc2 = (math.tau - angle) / (num_points - 1)

        if direction == "CW":
            angle_div = (arc1 if sc_angle > ec_angle else arc2) * -1
        else:
            angle_div = arc2 if sc_angle > ec_angle else arc1

        for i in range(num_points):
            cur_angle = sc_angle + i * angle_div
            p = Point.from_point(current)
            p[p1] = center[p1] + math.cos(cur_angle) * radius
            p[p2] = center[p2] + math.sin(cur_angle) * radius
            points.append(p)

        return points

    @staticmethod
    def cw(current: Point, end: Point, center: Point, num_points: int, plane: str="XY") -> List[Point]:
        """
        Clockwise arc move (G2)
        """

        return ArcMove._calculate(current, end, center, num_points, plane, "CW")

    @staticmethod
    def ccw(current: Point, end: Point, center: Point, num_points: int, plane: str="XY") -> List[Point]:
        """
        Counter-clockwise arc move (G3)
        """

        return ArcMove._calculate(current, end, center, num_points, plane, "CCW")
