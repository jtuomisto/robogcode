from typing import List, Union, Any, Callable


class Dependency:
    """
    Class for containing information about a dependency
    """

    PRIORITY_LOWEST = 10
    PRIORITY_HIGHEST = 0
    PRIORITY_DEFAULT = 5

    def __init__(self, data: Any, priority: int=None):
        self._data = data
        self._prio = int(priority) if priority is not None else Dependency.PRIORITY_DEFAULT

        if not Dependency.PRIORITY_HIGHEST <= self._prio <= Dependency.PRIORITY_LOWEST:
            raise ValueError(f"Invalid priority: {self._prio}")

    @property
    def data(self):
        return self._data

    @property
    def priority(self):
        return self._prio


class DependencyList:
    """
    Class for handling dependencies
    """

    def __init__(self):
        self._deps = []
        self._prio_index = {}

    def __len__(self):
        return len(self._deps)

    def _append(self, dep: Dependency) -> None:
        for d in self._deps:
            if d.data == dep.data:
                return

        if len(self._deps) == 0:
            self._deps.append(dep)
            self._prio_index[dep.priority] = 1
            return

        if dep.priority not in self._prio_index:
            deps_len = len(self._deps)
            index = 0

            while index < deps_len:
                if dep.priority < self._deps[index].priority:
                    break

                index += 1

            self._prio_index[dep.priority] = index

        self._deps.insert(self._prio_index[dep.priority], dep)
        self._prio_index[dep.priority] += 1

        for p in range(dep.priority + 1, Dependency.PRIORITY_LOWEST + 1):
            if p in self._prio_index:
                self._prio_index[p] += 1

    def append(self, dep: Union[Dependency, List[Dependency]]) -> None:
        if isinstance(dep, list):
            for d in dep:
                self._append(d)
        else:
            self._append(dep)

    def dependencies(self, map_function: Callable=lambda x: x) -> List[Any]:
        return [map_function(dep.data) for dep in self._deps]
