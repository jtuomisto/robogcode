from typing import Any
import math


class Values:
    """Transform values"""

    @staticmethod
    def to_bool(value: Any) -> bool:
        """Coerce value into boolean"""

        truthy = False

        if isinstance(value, str):
            lower = value.lower().strip()

            try:
                num = float(lower)
            except (ValueError, OverflowError) as e:
                truthy = lower == "true"
            else:
                truthy = num != 0
        elif isinstance(value, int) or isinstance(value, float):
            truthy = value != 0
        else:
            truthy = bool(value)

        return truthy

    @staticmethod
    def to_gcode(value: Any) -> str:
        """Transform value for insertion into gcode"""

        if isinstance(value, str):
            value = value.replace('"', '""')
            return f'"{value}"'
        elif isinstance(value, float):
            value_r = round(value, 3)

            if math.modf(value_r)[0] == 0:
                return str(int(value))
            else:
                return f"{value:.3f}"
        elif value is not None:
            return str(value)
        else:
            return ""
