import sys
import logging
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from robogcode import GCodeTranspiler
from typing import List
from robogcode.output import *
from robogcode.processor import *


LOG = None

OUTPUTS = {
    "gcode": GCodeOutput,
    "moto": MotomanOutput,
    "motoarc": MotomanArcOutput,
    "rapid": RAPIDOutput
}

# Not all processors are available from commandline,
# for example TrackPosition is not useful as it produces no output
PROCESSORS = {
    "join": LinearJoiner,
    "normalize": NormalizeUnits,
    "to_absolute": RelToAbs
}


def setup_logging() -> logging.Logger:
    logging.basicConfig(format="%(message)s")
    log = logging.getLogger("robogcode")
    log.setLevel(logging.INFO)
    return log


def handle_params(params: List[str]) -> dict:
    if params is None or len(params) == 0:
        return {}

    result = {}

    for p in params:
        key, _, value = p.partition("=")
        result[key] = value

    return result


def handle_processors(procs: List[str]) -> List:
    if procs is None or len(procs) == 0:
        return []

    return [PROCESSORS[p] for p in procs]


def handle_args(arguments: List[str]) -> object:
    outs = "Available output formats:\n{}\n\n".format(", ".join(OUTPUTS.keys()))
    procs = "Available processors:\n{}\n".format(", ".join(PROCESSORS.keys()))
    epilog = outs + procs
    parser = ArgumentParser(prog="robogcode",
                            description="Gcode transpiler",
                            formatter_class=RawDescriptionHelpFormatter,
                            epilog=epilog)

    parser.add_argument(
        "-i", "--input",
        required=True,
        metavar="FILE"
    )

    parser.add_argument(
        "-o", "--output",
        required=True,
        metavar="DIR"
    )

    parser.add_argument(
        "-f", "--format",
        required=True,
        help="Output format"
    )

    parser.add_argument(
        "-n", "--noextra",
        action="store_false",
        help="Do not copy extra data files"
    )

    parser.add_argument(
        "-p", "--processor",
        action="append",
        help="Preprocess gcode (can be specified multiple times)",
        metavar="PROC"
    )

    parser.add_argument(
        "-d", "--debug",
        action="store_true",
        help="Debug output"
    )

    parser.add_argument(
        "-P", "--param",
        nargs="+",
        help="Output parameters",
        metavar="KEY=VALUE"
    )

    return parser.parse_args(arguments)


def main():
    LOG = setup_logging()
    args = handle_args(sys.argv[1:])

    params = handle_params(args.param)
    procs = handle_processors(args.processor)

    gctrans = GCodeTranspiler(OUTPUTS[args.format](**params), procs, extras=args.noextra)
    gctrans.read(args.input)
    gctrans.write(args.output)
