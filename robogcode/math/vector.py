from __future__ import annotations
from math import sqrt
from typing import Tuple, Union, Any
from .vectorbase import VectorBase
from .helpers import get_from_data
from .point import Point


class Vector(VectorBase):
    """
    Container for vectors
    """

    @staticmethod
    def from_data(data: Any, defaults: Union[Vector, Tuple[float, float, float]]=None) -> Vector:
        """
        Create a Vector instance from data

        data should:
            have keys named i, j, k (case-insensitive)
        or:
            have attributes named i, j, k (case-insensitive)
        or:
            be a list/set/tuple of exactly three values
        """

        return get_from_data(Vector, "ijk", data, defaults)

    @staticmethod
    def from_vector(v: Vector) -> Vector:
        """Create a Vector from another Vector"""

        if not isinstance(v, Vector):
            raise TypeError("Not a Vector instance")

        return Vector(*v.as_tuple())

    @staticmethod
    def from_points(start: Point, end: Point) -> Vector:
        """Create a Vector from start and end point"""

        if not isinstance(start, Point) or not isinstance(end, Point):
            raise TypeError("Not Point instances")

        return Vector(end.x - start.x, end.y - start.y, end.z - start.z)

    def __init__(self, i: float=0, j: float=0, k: float=0):
        super().__init__("ijk", [i, j, k], float)

    def __repr__(self) -> str:
        return "Vector(" + super().__repr__() + ")"

    def dot(self, other: Vector) -> float:
        """Calculates the dot product of vectors"""

        if not isinstance(other, Vector):
            raise TypeError("Invalid type")

        return self.i * other.i + self.j * other.j + self.k * other.k

    def cross(self, other: Vector) -> Vector:
        """Calculates the cross product of vectors"""

        if not isinstance(other, Vector):
            raise TypeError("Invalid type")

        ia, ja, ka = self.as_tuple()
        ib, jb, kb = other.as_tuple()
        return Vector(ja * kb - ka * jb, ka * ib - ia * kb, ia * jb - ja * ib)

    @property
    def length(self) -> float:
        """Returns the length of the vector"""

        return sqrt(self.i**2 + self.j**2 + self.k**2)
