from __future__ import annotations
from typing import Tuple, Union, Any
from .vectorbase import VectorBase
from .helpers import get_from_data


class Point(VectorBase):
    """
    Container for xyz coordinates
    """

    @staticmethod
    def from_data(data: Any, defaults: Union[Point, Tuple[float, float, float]]=None) -> Point:
        """
        Create a Point instance from data

        data should:
            have keys named x, y, z (case-insensitive)
        or:
            have attributes named x, y, z (case-insensitive)
        or:
            be a list/set/tuple of exactly three values
        """

        return get_from_data(Point, "xyz", data, defaults)

    @staticmethod
    def from_point(p: Point) -> Point:
        """Create a Point from another Point"""

        if not isinstance(p, Point):
            raise TypeError("Not a Point instance")

        return Point(*p.as_tuple())

    def __init__(self, x: float=0, y: float=0, z: float=0):
        super().__init__("xyz", [x, y, z], float)

    def __repr__(self) -> str:
        return "Point(" + super().__repr__() + ")"
