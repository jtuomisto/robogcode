from __future__ import annotations
from math import sqrt
from typing import Any, Type, Union, Callable


class VectorBase:
    def __init__(self, names: str, values: list, value_type: Type):
        order = str(names).lower()
        value_count = len(order)

        if value_count == 0:
            raise ValueError("Invalid names")

        if len(values) != value_count:
            raise ValueError("Invalid amount of values")

        # Create instance variables this way to avoid infinite recursion
        self.__dict__["_order"] = order
        self.__dict__["_values"] = value_count
        # Dicts retain insertion order since 3.7
        self.__dict__["_map"] = {n: value_type(values[i]) for i, n in enumerate(self._order)}
        self.__dict__["_type"] = value_type

    def __len__(self) -> int:
        return self._values

    def __repr__(self) -> str:
        return ", ".join([f"{k}: {v}" for k, v in self._map.items()])

    def __getitem__(self, key: Union[str, int]) -> Any:
        k = self._get_key(key)
        return self._map[k]

    def __setitem__(self, key: Union[str, int], value: Any):
        k = self._get_key(key)
        self._map[k] = self._type(value)

    def __delitem__(self, key: Union[str, int]):
        k = self._get_key(key)
        self._map[k] = self._type(0)

    def __getattr__(self, name: str) -> Any:
        try:
            return self.__getitem__(name)
        except (IndexError, KeyError):
            raise AttributeError(f"No such attribute: {name}")

    def __setattr__(self, name: str, value: Any):
        try:
            self.__setitem__(name, value)
        except (IndexError, KeyError):
            raise AttributeError(f"No such attribute: {name}")

    def __delattr__(self, name: str):
        try:
            self.__delitem__(name)
        except (IndexError, KeyError):
            raise AttributeError(f"No such attribute: {name}")

    def __add__(self, other: Any) -> Any:
        if not isinstance(other, type(self)):
            return NotImplemented

        p = [sv + ov for sv, ov in zip(self._map.values(), other._map.values())]
        return type(self)(*p)

    def __sub__(self, other: Any) -> Any:
        if not isinstance(other, type(self)):
            return NotImplemented

        p = [sv - ov for sv, ov in zip(self._map.values(), other._map.values())]
        return type(self)(*p)

    def __mul__(self, mul: Any) -> Any:
        p = [sv * mul for sv in self._map.values()]
        return type(self)(*p)

    def __iadd__(self, other: Any) -> Any:
        if not isinstance(other, type(self)):
            return NotImplemented

        for k, v in other._map.items():
            self._map[k] += v

        return self

    def __isub__(self, other: Any) -> Any:
        if not isinstance(other, type(self)):
            return NotImplemented

        for k, v in other._map.items():
            self._map[k] -= v

        return self

    def __imul__(self, mul: Any) -> Any:
        for k in self._map.keys():
            self._map[k] *= mul

        return self

    def __eq__(self, other: Any) -> bool:
        return self.equal(other)

    def _get_key(self, key: Union[str, int]) -> str:
        if isinstance(key, int):
            if not 0 <= key < self._values:
                raise IndexError(f"Index out of bound: {key}")

            return self._order[key]
        elif isinstance(key, str):
            k = key.lower()

            if k not in self._map:
                raise KeyError(f"No such key: {key}")

            return k

        raise KeyError("Invalid key")

    def equal(self, other: Any, threshold: float=0.001) -> bool:
        if not isinstance(other, type(self)):
            return False

        if other is self:
            return True

        threshold = abs(threshold)
        values = zip(self._map.values(), other._map.values())

        if threshold == 0:
            compare = lambda v: v[0] == v[1]
        else:
            compare = lambda v: abs(v[0] - v[1]) <= threshold

        return all(map(compare, values))

    def as_tuple(self, formatter: Callable=lambda x: x) -> Tuple:
        return tuple([formatter(v) for v in self._map.values()])
