from typing import Tuple, Union, Callable, Any


def has_keys(keys, data):
    try:
        for k in keys:
            if k in data:
                return True
    except TypeError:
        return False

    return False


def has_attrs(attrs, data):
    for a in attrs:
        if hasattr(data, a):
            return True

    return False


def get_key_value(data, key, default):
    lower = key.lower()

    if key in data:
        return data[key]
    elif lower in data:
        return data[lower]

    return default


def get_attr_value(data, attr, default):
    lower = attr.lower()
    return getattr(data, attr, getattr(data, lower, default))


def get_from_data(classtype: Any, keys: str, data: Any, defaults: Union[Any, Tuple[float, float, float]]=None) -> Any:
    if defaults is None:
        defaults = (0, 0, 0)
    elif isinstance(defaults, classtype):
        defaults = defaults.as_tuple()

    inst = classtype(*defaults)
    kk1, kk2, kk3 = keys.upper()
    k1, k2, k3 = keys.lower()
    k = k1 + k2 + k3 + kk1 + kk2 + kk3

    if has_keys(k, data):
        inst[k1] = get_key_value(data, kk1, defaults[0])
        inst[k2] = get_key_value(data, kk2, defaults[1])
        inst[k3] = get_key_value(data, kk3, defaults[2])
    elif has_attrs(k, data):
        inst[k1] = get_attr_value(data, kk1, defaults[0])
        inst[k2] = get_attr_value(data, kk2, defaults[1])
        inst[k3] = get_attr_value(data, kk3, defaults[2])
    elif len(data) == 3:
        inst[k1] = data[0]
        inst[k2] = data[1]
        inst[k3] = data[2]

    return inst
