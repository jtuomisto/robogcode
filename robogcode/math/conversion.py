def _format(value, format_str, decimals):
    if format_str:
        return f"{value:.{decimals}f}"
    else:
        return value


class Conversion:
    @staticmethod
    def cm_min_mm_s(speed: float, format_str: str=False, decimals: int=1):
        """Converts speed in cm/min to mm/s"""

        speed = speed * 10 / 60
        return _format(speed, format_str, decimals)

    @staticmethod
    def mm_s_cm_min(speed: float, format_str: str=False, decimals: int=1):
        """Converts speed in mm/s to cm/min"""

        speed = speed * 60 / 10
        return _format(speed, format_str, decimals)

    @staticmethod
    def mm_min_mm_s(speed: float, format_str: str=False, decimals: int=1):
        """Converts mm/min to mm/s"""

        speed = speed / 60
        return _format(speed, format_str, decimals)

    @staticmethod
    def mm_inch(millis: float, format_str: str=False, decimals: int=1):
        """Converts millimeters to inches"""

        inches = millis / 25.399
        return _format(inches, format_str, decimals)

    @staticmethod
    def inch_mm(inches: float, format_str: str=False, decimals: int=1):
        """Converts inches to millimeters"""

        millis = inches * 25.399
        return _format(millis, format_str, decimals)
