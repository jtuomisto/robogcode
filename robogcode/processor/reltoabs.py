import logging
from .processor import Processor, ProcessorResult
from .normalizeunits import NormalizeUnits
from .trackposition import TrackPosition
from robogcode.math import Point
from robogcode.gcode import Plane


LOG = logging.getLogger(__name__)


class RelToAbs(Processor):
    """
    Transform relative coordinates to absolute coordinates
    """

    __deps__ = [NormalizeUnits, TrackPosition]
    __prio__ = 2

    def __init__(self):
        self._plane = Plane.XY
        self._lpoint = Point()
        self._cmd_names = ("G90", "G91")
        self._planes = { "G17": Plane.XY, "G18": Plane.ZX, "G19": Plane.YZ }
        self._move_names = ("G0", "G1", "G2", "G3")
        self._arc_names = ("G2", "G3")
        self._is_rel = False

    def __call__(self, cmd: str, params: dict) -> ProcessorResult:
        if cmd in self._cmd_names:
            self._is_rel = cmd == "G91"
            return ("G90", params)
        if cmd in self._planes.keys():
            self._plane = self._planes[cmd]
            return True
        elif cmd not in self._move_names:
            return True

        if self._is_rel:
            coords = self.get_meta(params, "TrackPosition.pos")
            x, y, z = coords.as_tuple()
            not_arc = cmd not in self._arc_names

            if not_arc or "X" in self._plane.value:
                params["X"] = x

            if not_arc or "Y" in self._plane.value:
                params["Y"] = y

            if not_arc or "Z" in self._plane.value:
                params["Z"] = z

            return (cmd, params)

        return True
