import logging
from math import sqrt, acos, degrees
from .processor import Processor, ProcessorResult
from .reltoabs import RelToAbs
from .trackposition import TrackPosition
from robogcode.math import Point, Vector


LOG = logging.getLogger(__name__)


class LinearJoiner(Processor):
    """
    Joins sequential linear moves (G1) together

    Will, obviously, lessen the accuracy of the moves
    """

    __deps__ = [TrackPosition, RelToAbs]

    DEGREE_THRESHOLD = 6  # Chosen after careful consideration

    def __init__(self):
        self._stack = []

    def __call__(self, cmd: str, params: dict) -> ProcessorResult:
        if cmd != "G1":
            return self._finish(cmd, params)

        has_coordinates = "X" in params or "Y" in params or "Z" in params
        has_frate_spindle = "F" in params or "S" in params

        if not has_coordinates or has_frate_spindle:
            return self._finish(cmd, params)

        coords = self.get_meta(params, "TrackPosition.pos")
        params["X"], params["Y"], params["Z"] = coords.as_tuple()

        if len(self._stack) < 2:
            self._stack.append((cmd, params))
            return False

        vec01 = self._get_vec(self._stack[0][1], self._stack[1][1])
        vec12 = self._get_vec(self._stack[1][1], params)
        angles = self._angle_diffs(vec01, vec12)

        if all(map(lambda a: a <= LinearJoiner.DEGREE_THRESHOLD, angles)):
            p = self._stack[1][1]

            for c in "XYZ":
                if c in p and c in params:
                    p[c] = params[c]

            if "E" in p or "E" in params:
                p["E"] = p.get("E", 0) + params.get("E", 0)

            return False
        else:
            return self._finish(cmd, params)

    def _get_vec(self, p1: dict, p2: dict) -> Point:
        return Vector.from_points(Point.from_data(p1), Point.from_data(p2))

    def _angle_diffs(self, vec01: Vector, vec12: Vector) -> float:
        diffs = []

        for a1, a2 in ("ij", "ki", "jk"):
            vec1 = Vector(**{ a1: vec01[a1], a2: vec01[a2] })
            vec2 = Vector(**{ a1: vec12[a1], a2: vec12[a2] })
            lenprod = vec1.length * vec2.length
            dotprod = vec1.dot(vec2)

            if lenprod != 0:
                diffs.append(degrees(acos(dotprod / lenprod)))
            else:
                diffs.append(0)

        return diffs

    def _finish(self, cmd: str, params: dict) -> ProcessorResult:
        if len(self._stack) > 0:
            ret = self._stack.copy()
            self._stack.clear()
            ret.append((cmd, params))
            return ret
        else:
            return True
