import logging
from .processor import Processor, ProcessorResult
from robogcode.math import Conversion, Point
from robogcode.gcode import Position, Units


LOG = logging.getLogger(__name__)


class TrackPosition(Processor):
    """
    Keeps track of current position
    """

    __prio__ = 1

    def __init__(self):
        self._pos = Position.ABSOLUTE
        self._unit = Units.MILLIMETER
        self._move_names = ("G0", "G1", "G2", "G3")
        self._point = Point()
        self._now = Point.from_point(self._point)
        self._before = Point.from_point(self._now)

    def __call__(self, cmd: str, params: dict) -> ProcessorResult:
        if cmd == "G90":
            self._pos = Position.ABSOLUTE
        elif cmd == "G91":
            self._pos = Position.RELATIVE
        elif cmd == "G20" and self._unit != Units.INCH:
            x, y, z = self._point.as_tuple(Conversion.mm_inch)
            self._point = Point(x, y, z)
            self._now = Point.from_point(self._point)
            self._unit = Units.INCH
        elif cmd == "G21" and self._unit != Units.MILLIMETER:
            x, y, z = self._point.as_tuple(Conversion.inch_mm)
            self._point = Point(x, y, z)
            self._now = Point.from_point(self._point)
            self._unit = Units.MILLIMETER
        elif cmd == "G28":
            axes = [a for a in params.keys() if a in "XYZ"]

            if len(axes) == 0:
                axes = "XYZ"

            x, y, z = self._point.as_tuple()
            self._point.x = 0 if "X" in axes else x
            self._point.y = 0 if "Y" in axes else y
            self._point.z = 0 if "Z" in axes else z
            self._now = Point.from_point(self._point)
        elif cmd in self._move_names:
            has_coords = "X" in params or "Y" in params or "Z" in params
            is_abs = self._pos == Position.ABSOLUTE

            if has_coords:
                defaults = self._point if is_abs else (0, 0, 0)
                p = Point.from_data(params, defaults)

                if is_abs:
                    self._point = p
                else:
                    self._point += p

                self._now = Point.from_point(self._point)

        self.set_meta(params, "TrackPosition.pos_last", self._before)
        self.set_meta(params, "TrackPosition.pos", self._now)

        self._before = Point.from_point(self._now)

        return True
