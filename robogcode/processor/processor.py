from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List, Union, Tuple, Any


class Processor(ABC):
    """
    Base class for gcode processors

    If the processor has dependecies, it should have a __deps__ class variable
    """

    # __deps__ = [Processor1, Processor2] # These should be *classes* not class instances
    # __prio__ = 5                  # Priority for this processor

    @staticmethod
    def set_meta(params: dict, key: str, value: Any) -> None:
        if "_meta" not in params:
            params["_meta"] = {}

        params["_meta"][key] = value

    @staticmethod
    def get_meta(params: dict, key: str) -> Any:
        if "_meta" not in params:
            raise KeyError(f"No metadata")

        meta = params["_meta"]

        if key not in meta:
            raise KeyError(f"No metadata with name: {key}")

        return meta[key]

    @abstractmethod
    def __call__(self, cmd: str, params: dict) -> ProcessorResult:
        """
        Return values:
        True:
            Command will stay unchanged
        False:
            Command will be removed
        Tuple(cmd, params), List[Tuple(cmd, params)]:
            Command will be replaced with these commands/parameters
        """

        raise NotImplementedError()


GCommand = Tuple[str, dict]
ProcessorResult = Union[List[GCommand], GCommand, bool]
ProcessorDeps = Union[Processor, List[Processor]]
