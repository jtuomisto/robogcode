import logging
from .processor import Processor, ProcessorResult
from robogcode.math import Conversion
from robogcode.misc import Dependency


LOG = logging.getLogger(__name__)


class NormalizeUnits(Processor):
    """
    Transform units into mm, mm/min

    G20 -> G21
    """

    __prio__ = Dependency.PRIORITY_HIGHEST

    def __init__(self):
        self._is_inches = False
        self._feed_inverse = False
        self._unit_names = ("G20", "G21")
        self._feed_names = ("G93", "G94")
        self._param_names = ("X", "Y", "Z", "I", "J", "K", "E", "F")

    def __call__(self, cmd: str, params: dict) -> ProcessorResult:
        if cmd in self._unit_names:
            self._is_inches = cmd == "G20"
            return ("G21", params)
        elif cmd in self._feed_names:
            self._feed_inverse = cmd == "G93"
            return True
        elif len(params) == 0:
            return True

        for p in self._param_names:
            value = params.get(p)

            if value is None:
                continue

            # We would need to know the length of the move here
            if self._feed_inverse and p == "F":
                continue

            if self._is_inches:
                params[p] = Conversion.inch_mm(value)

        return (cmd, params)
