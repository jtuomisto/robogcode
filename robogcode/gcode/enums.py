from enum import Enum


class Position(Enum):
    ABSOLUTE = 1
    RELATIVE = 2


class Units(Enum):
    MILLIMETER = 1
    INCH = 2


class Plane(Enum):
    XY = "XY"
    ZX = "ZX"
    YZ = "YZ"


class Feedrate(Enum):
    NORMAL = 1
    INVERSE = 2


class Group(Enum):
    ACTIVE      = "A"  # Active command
    MODE        = "M"  # Active modes
    MOTION      = 1    # Motion parameter values
    PLANE       = 2    # Plane selection
    POSITION    = 3    # Positioning type
    OFFSET_MODE = 4    # Arc offsets type
    FEED_MODE   = 5    # Feedrate mode
    UNITS       = 6    # Unit mode
    NONE        = 0    # No state
