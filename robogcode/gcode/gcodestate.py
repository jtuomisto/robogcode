from typing import Any, Union
from copy import deepcopy
from .enums import Group
from .structures import GCodeMode, GCodeParameter, GCodeCommand
from .gcodedefs import GCODE_COMMANDS


# Default gcode state
_GCODE_STATE = {
    Group.ACTIVE: None,                     # Active command
    Group.MODE: None,                       # Active modes
    Group.MOTION: {                         # Parameter values
        "X": GCodeParameter("X", 0.0),
        "Y": GCodeParameter("Y", 0.0),
        "Z": GCodeParameter("Z", 0.0),
        "A": GCodeParameter("A", None),
        "B": GCodeParameter("B", None),
        "C": GCodeParameter("C", None),
        "I": GCodeParameter("I", None),
        "J": GCodeParameter("J", None),
        "K": GCodeParameter("K", None),
        "R": GCodeParameter("R", None),
        "E": GCodeParameter("E", 0.0),
        "F": GCodeParameter("F", 500),
        "S": GCodeParameter("S", 0.0)
    },
    Group.PLANE:       Plane.XY,            # Plane selection
    Group.POSITION:    Position.ABSOLUTE,   # Positioning type
    Group.OFFSET_MODE: Position.RELATIVE,   # Arc offsets type
    Group.FEED_MODE:   Feedrate.NORMAL,     # Feedrate mode
    Group.UNITS:       Units.MILLIMETER     # Unit mode
}


class GCodeState:
    """Contains the current state (parameter values & modes) of the gcode program"""

    def __init__(self):
        self._state = deepcopy(_GCODE_STATE)

        mode = GCodeMode(self._state[Group.PLANE],
                         self._state[Group.POSITION],
                         self._state[Group.OFFSET_MODE],
                         self._state[Group.FEED_MODE],
                         self._state[Group.UNITS])

        self._state[Group.MODE] = mode

    def get(self, group: Group, param: Union[str, GCodeParameter]=None) -> Any:
        gr = Group(group)

        if gr is not Group.MOTION:
            return self._state[gr]

        if isinstance(param, GCodeParameter):
            param = param.name

        return self._state[gr][param]

    def set(self, command: GCodeCommand):
        if not isinstance(command, GCodeCommand):
            raise TypeError("Not a GCodeCommand")

        self._state[Group.ACTIVE] = command.name

        if command.name not in GCODE_COMMANDS:
            return

        gr = GCODE_COMMANDS[command.name]["group"]
        value = GCODE_COMMANDS[command.name]["value"]

        if gr is Group.MOTION:
            for param in command.parameters():
                self._state[gr][param.name] = param
        elif gr is not Group.NONE and value is not None:
            self._state[gr] = value
