from __future__ import annotations
from typing import Any, List
from math import modf
from .enums import Group
from .gcodedefs import GCODE, GCODE_COMMANDS, GCodeException


class GCodeMode:
    """Container for current modes (units, position modes, feed modes, etc.)"""

    __slots__ = ["plane", "position", "offset_mode", "feed_mode", "units"]

    def __init__(self, plane, pos, offs, feed, units):
        self.plane = plane
        self.position = pos
        self.offset_mode = offs
        self.feed_mode = feed
        self.units = units


class GCodeParameter:
    """Container for a gcode parameter"""

    __slots__ = ["_name", "_value", "_type", "_root", "_child"]

    def __init__(self, name: str, value: Any):
        self._name = name.upper()
        self._value = value
        self._child = None

        if self._name not in GCODE.PARAMS:
            raise GCodeException(f"Invalid parameter name: {self._name}")

        if isinstance(value, GCodeParameter):
            self._type = value._type
            self._root = value._root
            value._child = self
        else:
            self._type = type(value)
            self._root = self

    def __str__(self) -> str:
        value = self._root._value

        if isinstance(value, str):
            value = value.replace('"', '""')
            value = f'"{value}"'
        elif isinstance(value, float):
            value_r = round(value, 3)

            if modf(value_r)[0] == 0:  # No digits
                value = str(int(value))
            else:
                value = f"{value:.3f}"
        elif value is not None:
            value = str(value)
        else:
            value = ""

        return f"{self._name}{value}"

    @property
    def is_modal(self) -> bool:
        """Returns False if this parameter introduces a new value for a parameter"""

        return self._root is not self

    @property
    def name(self) -> str:
        """Returns the name of this parameter"""

        return self._name

    @property
    def type(self) -> Any:
        """Returns the type of this parameter"""

        return self._type

    @property
    def value(self) -> Any:
        """Returns the value of this parameter"""

        return self._root._value

    @value.setter
    def value(self, value: Any):
        if type(value) is not self._type:
            raise GCodeException(f"Incorrect value for parameter {self._name}: {value}")

        self._value = value

        if self._root is self:
            return

        self._root = self

        if self._child is not None:
            child = self._child

            while isinstance(child, GCodeParameter):
                child._root = self
                child = child._child


class GCodeCommand:
    """Container for a gcode command and its parameters"""

    __slots__ = ["_name", "_params", "_mode"]

    def __init__(self, name: str, mode: GCodeMode=None):
        self._name = name
        self._mode = mode if mode is not None else GCodeMode()
        self._params = {}

    def __getattr__(self, name: str) -> GCodeParameter:
        n = name.upper()

        if n not in self._params:
            raise GCodeException(f"Invalid parameter name: {name}")

        return self._params[n]

    def __getitem__(self, name: str) -> GCodeParameter:
        return self.__getattr__(name)

    def __str__(self) -> str:
        string = [self._name]
        string.extend([str(p) for p in self.parameters()])
        return " ".join(string)

    @property
    def name(self) -> str:
        """Returns the name of this command"""

        return self._name

    @property
    def group(self) -> str:
        """Returns the command group (G, M, T, or N)"""

        return self._name[0]

    @property
    def number(self) -> int:
        """Returns the command number (e.g. for 'G90' returns 90)"""

        return int(self._name[1:])

    @property
    def mode(self) -> GCodeMode:
        """Returns the mode state for this command"""

        return self._mode

    def parameters(self) -> GCodeCommandParamsIterator:
        """Returns an iterator for this commands parameters"""

        return GCodeCommandParamsIterator(self)

    def parameter(self, param: GCodeParameter):
        if self._name in GCODE_COMMANDS:
            pdef = GCODE_COMMANDS[self._name]["params"].get(param.name)

            if pdef is None:
                raise GCodeException(f"Invalid parameter for {self._name}: {param.name}")
            elif param.type is not pdef.type:
                raise GCodeException(f"Invalid parameter type for {self._name} {param.name}: {param.type}")

        self._params[param.name] = param


class GCodeCommandParamsIterator:
    def __init__(self, command: GCodeCommand):
        name = command._name
        self._params = command._params

        if name in GCODE_COMMANDS:
            self._order = GCODE_COMMANDS[name]["order"]
        else:
            self._order = self._params.keys()

        if self._order is None:
           self._order = ""

        self._index = -1
        self._len = len(order)

    def __iter__(self):
        return self

    def __next__(self):
        self._index += 1

        if self._index >= self._len:
            raise StopIteration()

        p = self._order[self._index]

        if p not in self._params:
            return self.__next__()
        else:
            return self._params[p]
