class Checksum:
    """Validate and generate checksums for gcode"""

    @staticmethod
    def generate(line: str) -> int:
        """
        Generate a checksum for a line

        line should not have eol characters or checksum
        """

        bytes_array = bytes(line, "ascii")
        cs = 0

        for b in bytes_array:
            cs ^= b

        return cs

    @staticmethod
    def validate(line: str, chksum: int) -> bool:
        """
        Validate a checksum for a line

        line should not have eol characters or checksum
        """

        return Checksum.generate(line) == chksum
