"""
Definitions for gcode
"""

from typing import Any, Type
from .enums import Plane, Position, Units, Feedrate, Group


class _:
    __slots__ = ["name", "type", "optional"]

    def __init__(self, name: str, param_type: Type=None, optional: bool=True):
        self.name = name
        self.type = param_type
        self.optional = optional


class GCodeException(Exception):
    pass


class GCODE:
    COMMANDS = "GMT"
    PARAMS = "XYZABCIJKREFSPDHN"
    COMMENTS = "(;"
    NUMBERS = "0123456789.-+"
    CHECK = "*"
    SKIP = "/"
    LINENUMBER = "N"
    LINE = "\r\n"


GCODE_COMMANDS = {
    "G0": { # Rapid linear move
        "group": Group.MOTION,
        "order": "XYZABCFS",
        "value": None,
        "params": {
            "X": _("X", float),
            "Y": _("Y", float),
            "Z": _("Z", float),
            "A": _("A", float),
            "B": _("B", float),
            "C": _("C", float),
            "F": _("F", float),
            "S": _("S", float)
        }
    },
    "G1": { # Linear move
        "group": Group.MOTION,
        "order": "XYZABCEFS",
        "value": None,
        "params": {
            "X": _("X", float),
            "Y": _("Y", float),
            "Z": _("Z", float),
            "A": _("A", float),
            "B": _("B", float),
            "C": _("C", float),
            "E": _("E", float),
            "F": _("F", float),
            "S": _("S", float)
        }
    },
    "G2": { # Clockwise arc move
        "group": Group.MOTION,
        "order": "XYZABCIJKREFS",
        "value": None,
        "params": {
            "X": _("X", float),
            "Y": _("Y", float),
            "Z": _("Z", float),
            "A": _("A", float),
            "B": _("B", float),
            "C": _("C", float),
            "I": _("I", float),
            "J": _("J", float),
            "K": _("K", float),
            "R": _("R", float),
            "E": _("E", float),
            "F": _("F", float),
            "S": _("S", float)
        }
    },
    "G3": { # Counter-clockwise arc move
        "group": Group.MOTION,
        "order": "XYZABCIJKREFS",
        "value": None,
        "params": {
            "X": _("X", float),
            "Y": _("Y", float),
            "Z": _("Z", float),
            "A": _("A", float),
            "B": _("B", float),
            "C": _("C", float),
            "I": _("I", float),
            "J": _("J", float),
            "K": _("K", float),
            "R": _("R", float),
            "E": _("E", float),
            "F": _("F", float),
            "S": _("S", float)
        }
    },
    "G4": { # Wait
        "group": Group.NONE,
        "order": "PS",
        "value": None,
        "params": {
            "P": _("P", int),
            "S": _("S", int)
        }
    },
    "G10": { # Retract
        "group": Group.NONE,
        "order": "S",
        "value": None,
        "params": {
            "S": _("S", int)
        }
    },
    "G11": { # Unretract
        "group": Group.NONE,
        "order": "S",
        "value": None,
        "params": {
            "S": _("S", int)
        }
    },
    "G17": { # Plane: XY
        "group": Group.PLANE,
        "order": None,
        "value": Plane.XY,
        "params": {}
    },
    "G18": { # Plane: ZX
        "group": Group.PLANE,
        "order": None,
        "value": Plane.ZX,
        "params": {}
    },
    "G19": { # Plane: YZ
        "group": Group.PLANE,
        "order": None,
        "value": Plane.YZ,
        "params": {}
    },
    "G20": { # Units: inches
        "group": Group.UNITS,
        "order": None,
        "value": Units.INCH,
        "params": {}
    },
    "G21": { # Units: millimeters
        "group": Group.UNITS,
        "order": None,
        "value": Units.MILLIMETER,
        "params": {}
    },
    "G28": { # Move to Origin
        "group": Group.NONE,
        "order": "XYZ",
        "value": None,
        "params": {
            "X": _("X"),
            "Y": _("Y"),
            "Z": _("Z")
        }
    },
    "G90": { # Absolute positioning
        "group": Group.POSITION,
        "order": None,
        "value": Position.ABSOLUTE,
        "params": {}
    },
    "G90.1": { # Absolute arc offsets
        "group": Group.OFFSET_MODE,
        "order": None,
        "value": Position.ABSOLUTE,
        "params": {}
    },
    "G91": { # Relative positioning
        "group": Group.POSITION,
        "order": None,
        "value": Position.RELATIVE,
        "params": {}
    },
    "G91.1": { # Relative arc offset
        "group": Group.OFFSET_MODE,
        "order": None,
        "value": Position.RELATIVE,
        "params": {}
    },
    "G93": { # Feed rate mode inverse (1/F)
        "group": Group.FEED_MODE,
        "order": None,
        "value": Feedrate.INVERSE,
        "params": {}
    },
    "G94": { # Feed rate mode (Units/Minute)
        "group": Group.FEED_MODE,
        "order": None,
        "value": Feedrate.NORMAL,
        "params": {}
    },
    "M2": { # Program end
        "group": Group.NONE,
        "order": None,
        "value": None,
        "params": {}
    },
    "M110": { # Set line number
        "group": Group.NONE,
        "order": "N",
        "value": None,
        "params": {
            "N": _("N", int, False)
        }
    }
}
