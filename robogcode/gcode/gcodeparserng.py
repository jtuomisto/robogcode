from __future__ import annotations
from typing import Any, List, Tuple, Union, Callable
import logging
import math
from .enums import Group
from .checksum import Checksum
from .structures import GCodeParameter, GCodeCommand, GCodeMode
from .gcodestate import GCodeState
from .gcodedefs import GCODE, GCODE_DEFINITION


LOG = logging.getLogger(__name__)



class GCodeParser:
    """
    Parses gcode

    NOTE:
    Comments inside parentheses are calculated as part of the checksum,
    if they appear before *. Is this correct?
    """

    def __init__(self):
        self._state = GCodeState()
        self._ln = 1  # Holds current line, for error messages

    def _validate(self, cmd: str, params: dict):
        if cmd not in GCODE_DEFINITION:
            # TODO: Implement optional strict mode where this is an error, maybe
            return



    def _fix(self, cmd: str, params: dict) -> Tuple[str, dict]:
        if cmd.startswith("T"):
            # Remove the tool number from the command,
            # put it into the params instead
            cmd = "T"
            params["T"] = int(cmd[1:])
        elif cmd == "M110":
            value = params["N"]

            if isinstance(value, float) and math.modf(value)[0] == 0:
                params["N"] = int(value)
            else:
                raise GCodeParserException(f"Invalid line number parameter at line {self._ln}")

        return (cmd, params)

    def _update_state(self, cmd: str, params: dict):
        pass

    def _string(self, data: str, index: int, length: int) -> Tuple[int, str]:
        string = None
        acc = []
        end = False
        start = index
        index += 1

        while index < length:
            char = data[index]

            if char == "\"":
                nextchar = data[index + 1] if index + 1 < length else None
                index += 1

                if nextchar == "\"":
                    acc.append(char)
                else:
                    end = True
                    break
            elif char in GCODE.LINE:
                raise GCodeParserException(f"Multiline/unclosed string at line {self._ln}")
            else:
                acc.append(char)

            index += 1

        if not end:
            raise GCodeParserException(f"Unclosed string at line {self._ln}")

        string = "".join(acc)
        return (index, string)

    def _number(self, data: str, index: int, length: int) -> Tuple[int, float]:
        number = None
        acc = []

        while index < length:
            char = data[index]

            if char in GCODE.NUMBERS:
                acc.append(char)
            else:
                break

            index +=1

        if len(acc) == 0:
            raise GCodeParserException(f"Invalid number at line {self._ln}")

        try:
            joined = "".join(acc)
            number = float(joined)
        except (ValueError, OverflowError):
            raise GCodeParserException(f"Invalid number at line {self._ln}: {joined}")

        return (index, number)

    def _param(self, data: str, index: int, length: int, param_names: str=None) -> Tuple[int, GCodeParameter]:
        if param_names is None:
            param_names = GCODE.PARAMS

        name = None
        value = None

        while index < length:
            char = data[index].upper()

            if char.isspace() or char in GCODE.COMMENTS or char in GCODE.CHECK:
                break
            elif char in param_names and name is None:
                name = char
                index += 1
            elif char in GCODE.NUMBERS and value is None:
                index, value = self._number(data, index, length)
                break
            elif char == "\"" and value is None:
                index, value = self._string(data, index, length)
                break
            else:
                raise GCodeParserException(f"Unexpected character in parameter at line {self._ln}: {char}")

        return (index, GCodeParameter(name, value))

    def _cmd(self, data: str, index: int, length: int) -> Tuple[int, str]:
        name = None
        has_dot = False
        acc = []

        while index < length:
            char = data[index].upper()

            if char.isspace() or char in GCODE.CHECK:
                break
            elif char in GCODE.COMMANDS and len(acc) == 0:
                acc.append(char)
            elif char.isdecimal() and len(acc) > 0:
                nextchar = data[index + 1] if index + 1 < length else "None"

                # No zero padded commands (eg. M05 should be translated to M5)
                if len(acc) >= 2 or (char != "0" or not nextchar.isdecimal()):
                    acc.append(char)
            elif char == "." and len(acc) >= 2 and not has_dot:
                acc.append(char)
                has_dot = True
            else:
                raise GCodeParserException(f"Invalid character at line {self._ln}: {char}")

            index += 1

        if len(acc) < 2 or acc[-1] == ".":
            raise GCodeParserException(f"Invalid command at line {self._ln}")

        name = "".join(acc)
        return (index, name)

    def _command(self, data: str, index: int, length: int) -> Tuple[int, GCodeCommand, List[str]]:
        cmd = None
        params = {}
        comments = []

        while index < length:
            char = data[index].upper()

            if char in GCODE.LINE or char in GCODE.CHECK:
                break
            elif char.isspace():
                index += 1
            elif char in GCODE.COMMENTS:
                index, comment = self._comment(data, index, length)
                comments.append(comment)
            elif char in GCODE.COMMANDS:
                if cmd is not None:
                    if len(params) > 0:
                        raise GCodeParserException(f"Several commands at line {self._ln}")
                    else:
                        break
                else:
                    index, cmd = self._cmd(data, index, length)
            elif char in GCODE.PARAMS and cmd is not None:
                index, param = self._param(data, index, length)

                if param.name in params:
                    raise GCodeParserException(f"Duplicate parameter at line {self._ln}: {name}")

                params[param.name] = param
            elif cmd == "M110" and char in GCODE.LINENUMBER and len(params) == 0:
                # M110 takes line number as a parameter
                index, param = self._param(data, index, length, GCODE.LINENUMBER)
                params[param.name] = param
                break
            else:
                raise GCodeParserException(f"Invalid character in command at line {self._ln}")

        if cmd is None:
            raise GCodeParserException(f"Invalid command at line {self._ln}")

        self._validate(cmd, params)
        cmd, params = self._fix(cmd, params)
        self._update_state(cmd, params)

        command = GCodeCommand(cmd, params.values(), self._state.current())
        return (index, command, comments)

    def _comment(self, data: str, index: int, length: int) -> Tuple[int, str]:
        start = data[index]
        end = ")" if start == "(" else "\n"
        comment = ""
        acc = []
        index += 1

        while index < length:
            char = data[index]

            if char == end:
                if start == "(":
                    index += 1

                break
            elif start == "(" and char in GCODE.LINE:
                raise GCodeParserException(f"Multiline comment at line {self._ln}")
            elif char not in GCODE.LINE:
                acc.append(char)

            index += 1

        if len(acc) > 0:
            comment = "".join(acc)

        return (index, comment)

    def _line_number(self, gcln: int, data: str, index: int, length: int) -> Tuple[int, int]:
        last_ln = gcln
        acc = []

        while index < length:
            char = data[index].upper()

            if char.isspace() or char in GCODE.COMMENTS or char in GCODE.CHECK:
                break
            elif (char == "N" and len(acc) == 0) or (char.isdecimal() and len(acc) > 0):
                acc.append(char)
            else:
                raise GCodeParserException(f"Unexpected character in line number at line {self._ln}")

            index += 1

        ln = "".join(acc)

        if len(ln) < 2:
            raise GCodeParserException(f"Invalid line number at line {self._ln}")

        gcln = int(ln[1:])

        if gcln < 0:
            raise GCodeParserException(f"Invalid line number at line {self._ln}")
        elif gcln <= last_ln:
            raise GCodeParserException(f"Wrong order of line numbers at line {self._ln}")

        return (index, gcln)

    def _checksum(self, data: str, start: int, current: int, length: int) -> int:
        index = start
        end = current

        while end < length:
            char = data[end]

            if char not in GCODE.CHECK and not char.isdecimal():
                break

            end += 1

        line = data[start:current]
        chksum = data[current:end]

        if not Checksum.validate(line, int(chksum[1:])):
            raise GCodeParserException(f"Invalid checksum at line {self._ln}: {chksum}")

        return end

    def _skip(self, data: str, index: int, length: int) -> int:
        while index < length:
            if data[index] == "\n":
                break

            index += 1

        return index

    def parse(self, data: str, command_hook: Callable, comment_hook: Callable=None) -> None:
        """
        Parse gcode

        command_hook should be a callable of type:
            def command_hook(command: GCodeCommand)

        comment_hook should be a callable of type:
            def comment_hook(comment: str)

        Return values of hooks are discarded
        """

        index = 0
        start = 0
        length = len(data)
        gcode_ln = -1
        self._ln = 1

        LOG.info("Parsing gcode...")

        while index < length:
            char = data[index].upper()

            if char.isspace():
                nextchar = data[index + 1] if index + 1 < length else None

                if char == "\r" and nextchar != "\n":
                    raise GCodeParserException(f"Invalid line ending at line {self._ln}")

                index += 1

                if char == "\n":
                    start = index
                    self._ln += 1
            elif char in GCODE.SKIP:
                index = self._skip(data, index, length)
            elif char in GCODE.LINENUMBER:
                index, gcode_ln = self._line_number(gcode_ln, data, index, length)
            elif char in GCODE.CHECK:
                index = self._checksum(data, start, index, length)
            elif char in GCODE.COMMENTS:
                index, comment = self._comment(data, index, length)

                if comment_hook is not None:
                    comment_hook(comment)
            elif char in GCODE.COMMANDS:
                index, command, comments = self._command(data, index, length)
                command_hook(command)

                if comment_hook is not None:
                    for comment in comments:
                        comment_hook(comment)

                # Handle line number set
                if cmd == "M110" and "N" in params:
                    gcode_ln = params["N"]
            else:
                raise GCodeParserException(f"Unexpected character at line {self._ln}: {char}")
