MODULE gcode
    LOCAL VAR robtarget target;

    FUNC robtarget grb(num x, num y, num z)
        VAR robtarget p;
        p:=target;
        p.trans.x:=x;
        p.trans.y:=y;
        p.trans.z:=z;
        RETURN p;
    ENDFUNC

    FUNC speeddata gspd(num speed)
        RETURN [speed, 500, 5000, 1000];
    ENDFUNC

    PROC gcode_init(PERS tooldata tool, PERS wobjdata wobj)
        target:=CRobT(\Tool:=tool \WObj:=wobj);
        target.trans.x:=0;
        target.trans.y:=0;
        target.trans.z:=0;
    ENDPROC
ENDMODULE
