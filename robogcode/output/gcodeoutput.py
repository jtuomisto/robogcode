import logging
from functools import partial as bind
from .robotoutput import RobotOutput, RobotResult
from robogcode.misc import Values
from robogcode.gcode import Checksum


LOG = logging.getLogger(__name__)


class GCodeOutput(RobotOutput):
    """
    Outputs gcode (surprise)
    """

    def __init__(self, filename: str="output.gcode", line_numbers: bool=False, checksums: bool=False):
        self._filename = str(filename)
        self._lines = Values.to_bool(line_numbers)
        self._sums = Values.to_bool(checksums)
        self._ln = 0

    def __getattr__(self, name: str):
        if name[0] not in "GMT":
            raise AttributeError(f"No attribute named {name}")

        return bind(self._func, name)

    def _func(self, cmd: str, params: str) -> RobotResult:
        name = cmd.replace("_", ".")
        line = []

        if name == "T":  # Fix tool command
            t = params["T"]
            line.append(f"T{t}")
        else:
            line.append(name)

            for key, value in params.items():
                if not key.startswith("_"):
                    gcvalue = Values.to_gcode(value)
                    line.append(f"{key}{gcvalue}")

        if self._lines:
            line.insert(0, f"N{self._ln}")
            self._ln += 1

            if name == "M110":  # Set line number
                self._ln = params["N"] + 1

        ln = " ".join(line)

        if self._sums:
            chk = Checksum.generate(ln)
            return f"{ln}*{chk}"

        return ln

    def pre(self, nfile: int=0) -> RobotResult:
        return None

    def post(self, nfile: int=0, last: bool=True) -> RobotResult:
        return None

    def linelimit(self) -> int:
        return None

    def encoding(self) -> str:
        return "ascii"

    def filename(self, nfile: int=0) -> str:
        return self._filename

    def eol(self) -> str:
        return "\r\n"

    G0  = lambda self, params: self._func("G0",  params)
    G1  = lambda self, params: self._func("G1",  params)
    G2  = lambda self, params: self._func("G2",  params)
    G3  = lambda self, params: self._func("G3",  params)
    G10 = lambda self, params: self._func("G10", params)
    G11 = lambda self, params: self._func("G11", params)
    G17 = lambda self, params: self._func("G17", params)
    G18 = lambda self, params: self._func("G18", params)
    G19 = lambda self, params: self._func("G19", params)
    G20 = lambda self, params: self._func("G20", params)
    G21 = lambda self, params: self._func("G21", params)
    G28 = lambda self, params: self._func("G28", params)
    G90 = lambda self, params: self._func("G90", params)
    G91 = lambda self, params: self._func("G91", params)
    M2  = lambda self, params: self._func("M2",  params)
