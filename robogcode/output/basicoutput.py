import logging
from .robotoutput import RobotOutput, RobotResult
from robogcode.processor import Processor, TrackPosition, NormalizeUnits
from robogcode.math import Conversion, Point
from robogcode.gcode import Position, Plane


LOG = logging.getLogger(__name__)


class BasicOutput(RobotOutput):
    """
    Base class for Motoman & RAPID outputs
    """

    __deps__ = [NormalizeUnits, TrackPosition]

    RETRACT_HEIGHT = 25    # mm
    DEFAULT_SPEED = 166.6  # mm/s
    THRESHOLD = 0.001      # mm

    def __init__(self):
        self._pos = Position.ABSOLUTE
        self._plane = Plane.XY
        self._retract = 0
        self._coords = Point()
        self._speed = BasicOutput.DEFAULT_SPEED
        self._frmt_mult = 1
        self._frmt_dec = 3

    def _formatter(self, v: float) -> str:
        if abs(v) <= BasicOutput.THRESHOLD:
            return "0"

        return f"{v * self._frmt_mult:.{self._frmt_dec}f}"

    def _update_point(self, params: dict) -> None:
        self._coords = Processor.get_meta(params, "TrackPosition.pos")
        self._coords.z += self._retract

    def _save(self, params: dict) -> None:
        if "F" in params:
            self._speed = Conversion.mm_min_mm_s(params["F"])

        self._update_point(params)

    def _has_coords(self, params: dict) -> bool:
        return "X" in params or "Y" in params or "Z" in params

    def G10(self, params: dict) -> RobotResult:
        if self._retract != 0:
            return None

        self._retract = BasicOutput.RETRACT_HEIGHT
        self._update_point(params)

        return True

    def G11(self, params: dict) -> RobotResult:
        if self._retract == 0:
            return None

        self._retract = 0
        self._update_point(params)

        return True

    def G17(self, params: dict) -> RobotResult:
        self._plane = Plane.XY
        return None

    def G18(self, params: dict) -> RobotResult:
        self._plane = Plane.ZX
        return None

    def G19(self, params: dict) -> RobotResult:
        self._plane = Plane.YZ
        return None

    def G20(self, params: dict) -> RobotResult:
        return None

    def G21(self, params: dict) -> RobotResult:
        return None

    def G28(self, params: dict) -> RobotResult:
        self._update_point(params)

        axes = [a for a in params.keys() if a in "XYZ"]

        if len(axes) == 0 or "Z" in axes:
            self.G11(params)

    def G90(self, params: dict) -> RobotResult:
        self._pos = Position.ABSOLUTE
        return None

    def G91(self, params: dict) -> RobotResult:
        self._pos = Position.RELATIVE
        return None

    def M2(self, params: dict) -> RobotResult:
        return None
