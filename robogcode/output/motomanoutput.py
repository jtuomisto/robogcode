import logging
from typing import List
from datetime import datetime
from .basicoutput import BasicOutput
from .robotoutput import RobotOutput, RobotResult
from robogcode.processor import Processor
from robogcode.math import Conversion, Point, Vector
from robogcode.misc import ArcMove
from robogcode.gcode import Position


LOG = logging.getLogger(__name__)


# NOTE:
# P127 = origin with orientations
# P126 = all zeros
# LP000 = home point
# LP001 = retract
# LP002 = unretract
# P120 - P124 = Arc move coordinates
class MotomanOutput(BasicOutput):
    """Produces code suitable for Yaskawa controllers"""

    SPLINE_THRESHOLD = 5 # mm

    def __init__(self, job_name: str=None, user_frame: int=1, tool: int=0):
        """
        Initialize MotomanOutput

        user_frame:  user frame number (1-63)
        tool:        tool number (0-63)
        """

        super().__init__()

        self._user_frame = int(user_frame)
        self._tool = int(tool)

        self._frmt_mult = 1000
        self._frmt_dec = 0

        self._time = datetime.now()
        self._frate_speed = Conversion.mm_s_cm_min(self._speed, True, 0)

        if job_name is None:
            time = self._time.strftime("%H%M")
            job_name = f"gcode{time}"

        self._job = str(job_name).upper()

    def _call(self, jobname: str, arguments: list=[]) -> str:
        args = [f"ARGF{a}" for a in arguments]

        if len(args) > 0:
            args = " " + " ".join(args)
        else:
            args = ""

        return f"CALL JOB:{jobname}{args}"

    def _save(self, params: dict) -> None:
        super()._save(params)

        if "F" in params:
            self._frate_speed = Conversion.mm_s_cm_min(self._speed, True, 0)

    def _jobname(self, nfile: int=0) -> str:
        nfile = nfile if nfile > 0 else ""
        return f"{self._job}{nfile}"

    def _G23(self, params: dict, func) -> RobotResult:
        self._save(params)

        code = []

        if not self._has_coords(params):
            return code

        coords = Processor.get_meta(params, "TrackPosition.pos_last")
        end = Processor.get_meta(params, "TrackPosition.pos")
        offs = Point(params.get("I", 0), params.get("J", 0), params.get("K", 0))
        center = coords + offs
        points = func(coords, end, center, 5, self._plane.value)
        pos_index = 120  # Make this less magic

        for p in points:
            x, y, z = p.as_tuple(self._formatter)
            args = [x, y, z, pos_index]
            pos_index += 1
            code.append(self._call("G23POS", args))

        code.append(self._call("G23", [self._frate_speed]))

        return code

    def pre(self, nfile: int=0) -> RobotResult:
        job = self._jobname(nfile)
        time = self._time.strftime("%Y/%m/%d %H:%M")
        uf = self._user_frame
        tl = self._tool
        retr = MotomanOutput.RETRACT_HEIGHT * 1000
        unretr = MotomanOutput.RETRACT_HEIGHT * -1000

        return [
            "/JOB",
            f"//NAME {job}",
            "//POS",
            "///NPOS 0,0,0,0,0,0",
            f"///TOOL {tl}",
            "//INST",
            f"///DATE {time}",
            "///ATTR SC,RW",
            "///GROUP1 RB1",
            "///LVARS 0,0,0,0,0,3,0,0",
            "NOP",
            "GETS PX127 $PX000",
            f"CNVRT PX127 PX127 UF#({uf}) TL#({tl})",
            "SET P126 P127",
            "SUB P126 P126",
            "SETE P127 (1) 0",
            "SETE P127 (2) 0",
            "SETE P127 (3) 0",
            "SET LP000 P127",
            "SET LP001 P126",
            "SET LP002 P126",
            f"SETE LP001 (3) {retr:.0f}",
            f"SETE LP002 (3) {unretr:.0f}"
        ]

    def post(self, nfile: int=0, last: bool=True) -> RobotResult:
        code = []

        if not last:
            next_job = self._jobname(nfile + 1)
            code.append(self._call(next_job))

        code.append("END")
        return code

    def linelimit(self) -> int:
        return None

    def files(self) -> List[str]:
        return ["moto/G01I.JBI",
                "moto/G01L.JBI",
                "moto/G01S.JBI",
                "moto/G23.JBI",
                "moto/G23POS.JBI"]

    def encoding(self) -> str:
        return "ascii"

    def filename(self, nfile: int=0) -> str:
        job = self._jobname(nfile)
        return f"{job}.JBI"

    def eol(self) -> str:
        return "\r\n"

    def G0(self, params: dict) -> RobotResult:
        self._save(params)

        code = []
        last = Processor.get_meta(params, "TrackPosition.pos_last")

        if not self._has_coords(params):
            return code

        if self._pos == Position.RELATIVE:
            p = Point.from_data(params)
        else:
            p = self._coords

        vec = Vector.from_points(last, self._coords)
        x, y, z = p.as_tuple(self._formatter)
        args = [x, y, z, self._frate_speed]

        if self._pos == Position.RELATIVE:
            mov = "G01I"
        elif not last.equal(self._coords) and vec.length < MotomanOutput.SPLINE_THRESHOLD:
            mov = "G01S"  # MOVS on short moves
        else:
            mov = "G01L"

        code.append(self._call(mov, args))
        return code

    def G1(self, params: dict) -> RobotResult:
        return self.G0(params)

    def G2(self, params: dict) -> RobotResult:
        return self._G23(params, ArcMove.cw)

    def G3(self, params: dict) -> RobotResult:
        return self._G23(params, ArcMove.ccw)

    def G10(self, params: dict) -> RobotResult:
        code = super().G10(params)

        if code is None:
            return None

        return f"IMOV LP001 V={self._frate_speed} PL=1"

    def G11(self, params: dict) -> RobotResult:
        code = super().G11(params)

        if code is None:
            return None

        return f"IMOV LP002 V={self._frate_speed} PL=1"

    def G28(self, params: dict) -> RobotResult:
        super().G28(params)

        x, y, z = self._coords.as_tuple(self._formatter)

        return [
            "SET LP000 P127",
            f"SETE LP000 (1) {x}",
            f"SETE LP000 (2) {y}",
            f"SETE LP000 (3) {z}",
            f"MOVL LP000 V={self._frate_speed} PL=1"
        ]
