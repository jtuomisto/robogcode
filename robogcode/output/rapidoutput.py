import logging
from typing import List
from .basicoutput import BasicOutput
from .robotoutput import RobotOutput, RobotResult
from robogcode.processor import Processor, TrackPosition, NormalizeUnits, RelToAbs
from robogcode.math import Point
from robogcode.misc import ArcMove


LOG = logging.getLogger(__name__)


class RAPIDOutput(BasicOutput):
    """
    Produces code for ABB controllers
    """

    __deps__ = [TrackPosition, NormalizeUnits, RelToAbs]

    def __init__(self, module_name: str="GcodeModule", wobj: str="wobj0", tool: str="tool0"):
        super().__init__()

        self._module_name = str(module_name)
        self._wobj = str(wobj)
        self._tool = str(tool)
        self._zone = "z0"

        self._frmt_mult = 1
        self._frmt_dec = 3

        self._frate_speed = self._formatter(self._speed)

    def _movel(self, p: Point) -> str:
        x, y, z = p.as_tuple(self._formatter)
        s = self._frate_speed
        zz = self._zone
        t = self._tool
        w = self._wobj

        return f"MoveL grb({x}, {y}, {z}), gspd({s}), {zz}, {t} \WObj:={w};"

    def _movec(self, p1: Point, p2: Point) -> str:
        x1, y1, z1 = p1.as_tuple(self._formatter)
        x2, y2, z2 = p2.as_tuple(self._formatter)
        s = self._frate_speed
        zz = self._zone
        t = self._tool
        w = self._wobj

        return f"MoveC grb({x1}, {y1}, {z1}), grb({x2}, {y2}, {z2}), gspd({s}), {zz}, {t} \WObj:={w};"

    def _save(self, params: dict) -> None:
        super()._save(params)

        if "F" in params:
            self._frate_speed = self._formatter(self._speed)

    def _G23(self, params: dict, func) -> RobotResult:
        self._save(params)

        if not self._has_coords(params):
            return None

        coords = Processor.get_meta(params, "TrackPosition.pos_last")
        end = Processor.get_meta(params, "TrackPosition.pos")
        offs = Point(params.get("I", 0), params.get("J", 0), params.get("K", 0))
        center = coords + offs
        _, p1, p2, p3, p4 = func(coords, end, center, 5, self._plane.value)

        return [self._movec(p1, p2), self._movec(p3, p4)]

    def pre(self, nfile: int=0) -> RobotResult:
        m = self._module_name
        t = self._tool
        w = self._wobj

        return [
            f"MODULE {m}",
            "PROC main()",
            f"gcode_init {t}, {w};"
        ]

    def post(self, nfile: int=0, last: bool=True) -> RobotResult:
        return [
            "ENDPROC",
            "ENDMODULE"
        ]

    def linelimit(self) -> int:
        return None

    def files(self) -> List[str]:
        return ["rapid/gcode.mod"]

    def encoding(self) -> str:
        return "utf8"

    def filename(self, nfile: int=0) -> str:
        return f"{self._module_name}.mod"

    def eol(self) -> str:
        return "\r\n"

    def G0(self, params: dict) -> RobotResult:
        self._save(params)

        if not self._has_coords(params):
            return None

        return self._movel(self._coords)

    def G1(self, params: dict) -> RobotResult:
        return self.G0(params)

    def G2(self, params: dict) -> RobotResult:
        return self._G23(params, ArcMove.cw)

    def G3(self, params: dict) -> RobotResult:
        return self._G23(params, ArcMove.ccw)

    def G10(self, params: dict) -> RobotResult:
        code = super().G10(params)

        if code is None:
            return None

        return self._movel(self._coords)

    def G11(self, params: dict) -> RobotResult:
        code = super().G11(params)

        if code is None:
            return None

        return self._movel(self._coords)

    def G28(self, params: dict) -> RobotResult:
        super().G28(params)

        return self._movel(self._coords)
