import logging
from typing import List
from .robotoutput import RobotResult
from .motomanoutput import MotomanOutput


LOG = logging.getLogger(__name__)


class MotomanArcOutput(MotomanOutput):
    """
    Produces code suitable for Yaskawa controllers (Arc welding)
    """

    def __init__(self, job_name: str=None, user_frame: int=1, tool: int=0, arc_file: int=1, weld: str="E"):
        """
        Initialize MotomanArcOutput

        arc_file:  arc start file (1-1000)
        weld:      when to weld (either E or G)
                   E: weld when E parameter is present
                   G: weld during G1/2/3
        """

        super().__init__(job_name, user_frame, tool)
        self._arc_file = int(arc_file)
        self._weld = str(weld).upper()
        self._arc_on = False

        if self._weld not in ("E", "G"):
            raise ValueError(f"Invalid weld value: {weld}")

    def __getattr__(self, name: str) -> str:
        # Welding should be stopped when we hit an unhandled command
        if self._arc_on:
            self._arc_on = False
            return lambda params: "ARCOF"

        raise AttributeError()

    def _arc(self, cmd: str, params: dict, code: List[str]):
        has_coords = len(code) > 0

        if self._weld == "E":
            weld = "E" in params
        else:
            weld = cmd in ("G1", "G2", "G3")

        if weld and has_coords and not self._arc_on:
            self._arc_on = True
            code.insert(0, f"ARCON ASF#({self._arc_file})")
        elif (not weld or not has_coords) and self._arc_on:
            self._arc_on = False
            code.insert(0, "ARCOF")

    def pre(self, nfile: int=0) -> RobotResult:
        code = super().pre(nfile)
        # Before arc can be started on a motoman, one step has to be present
        # This doesnt actually move anywhere since P126 is all zeros
        code.append("IMOV P126")
        return code

    def post(self, nfile: int=0, last: bool=True) -> RobotResult:
        code = super().post(nfile, last)

        if last and self._arc_on:
            self._arc_on = False
            # Insert ARCOF before END
            code.insert(len(code) - 1, "ARCOF")

        return code

    def G0(self, params: dict) -> RobotResult:
        code = super().G0(params)

        if self._arc_on:
            self._arc_on = False
            code.insert(0, "ARCOF")

        return code

    def G1(self, params: dict) -> RobotResult:
        code = super().G0(params)
        self._arc("G1", params, code)
        return code

    def G2(self, params: dict) -> RobotResult:
        code = super().G2(params)
        self._arc("G2", params, code)
        return code

    def G3(self, params: dict) -> RobotResult:
        code = super().G3(params)
        self._arc("G3", params, code)
        return code

    def G10(self, params: dict) -> RobotResult:
        code = super().G10(params)
        code = [code] if code is not None else []

        if self._arc_on:
            self._arc_on = False
            code.insert(0, "ARCOF")

        return code

    def G28(self, params: dict) -> RobotResult:
        code = super().G28(params)

        if self._arc_on:
            self._arc_on = False
            code.insert(0, "ARCOF")

        return code
