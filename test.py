import sys
import os
import logging
import shutil
from traceback import print_exception
from pathlib import Path
from robogcode import GCodeTranspiler
from robogcode.gcode import GCodeParser
from robogcode.output import *
from robogcode.processor import *


TEST_OUTPUT = None
TESTS = []


def test_runner(num, fail_type, filename, output, outargs, processors):
    print(f"===TEST {num}===")
    should_fail = fail_type is not None
    fail_type = Exception if not should_fail else fail_type
    correct_fail = True
    failed = False
    ex = None

    try:
        try:
            gctrans = GCodeTranspiler(output(**outargs), processors)
            gctrans.read(filename)
            gctrans.write(str(TEST_OUTPUT))
        except fail_type as e:
            failed = True
            correct_fail = should_fail
            ex = e
        else:
            correct_fail = not should_fail
    except Exception as e:
        failed = True
        correct_fail = False
        ex = e

    if not correct_fail:
        if failed:
            print_exception(type(ex), ex, ex.__traceback__)

        print(f"===TEST {num} FAILED===", end="\n\n")
        return False
    else:
        print(f"===TEST {num} OK===", end="\n\n")
        return True


def test(fail_type, filename, output, outargs={}, processors=None):
    TESTS.append([len(TESTS) + 1, fail_type, filename, output, outargs, processors])


def run_tests(argv):
    if len(argv) == 0:
        for args in TESTS:
            if not test_runner(*args):
                break

        return

    test_count = len(TESTS)

    for num in argv:
        n = int(num) - 1

        if 0 <= n < test_count:
            if not test_runner(*TESTS[n]):
                break


def get_path():
    outpath = Path(__file__).parent / "test_result"
    return outpath.absolute()


def create_dirs():
    outdir = str(TEST_OUTPUT)

    if not TEST_OUTPUT.exists():
        os.mkdir(outdir, mode=0o755)


def clean():
    shutil.rmtree(str(TEST_OUTPUT), ignore_errors=True)


# <TESTS>
def test_parser():
    def command(cmd, params):
        print(f"  Command: '{cmd}'")

        for p, v in params.items():
            print(f"    '{p}' {type(v)!r}: {v}")


    def comment(cmt):
        print(f"  Comment: '{cmt}'")

    gcparser = GCodeParser()
    files = ["test_data/parser_test.gcode",
             "test_data/linear_test.gcode",
             "test_data/processor_test.gcode"]

    for f in files:
        print(f"FILE: {f}")

        with open(f, "r") as handle:
            gcparser.parse(handle.read(), command, comment)

        print()


def tests():
    test( # TEST 1
        fail_type=None,
        filename="test_data/parser_test.gcode",
        output=GCodeOutput,
        outargs={"filename": "01_parser_test_parsed.gcode"}
    )

    test( # TEST 2
        fail_type=None,
        filename="test_data/parser_test_2.gcode",
        output=GCodeOutput,
        outargs={"filename": "02_parser_test_2_parsed.gcode"}
    )

    test( # TEST 3
        fail_type=None,
        filename="test_data/parser_test_3.gcode",
        output=GCodeOutput,
        outargs={"filename": "03_parser_test_3_parsed.gcode"}
    )

    test( # TEST 4
        fail_type=None,
        filename="test_data/sedu.gcode",
        output=GCodeOutput,
        outargs={"filename": "04_sedu_linear.gcode"},
        processors=LinearJoiner
    )

    test( # TEST 5
        fail_type=None,
        filename="test_data/processor_test.gcode",
        output=GCodeOutput,
        outargs={"filename": "05_processor_test_normalized.gcode"},
        processors=NormalizeUnits
    )

    test( # TEST 6
        fail_type=None,
        filename="test_data/processor_test.gcode",
        output=GCodeOutput,
        outargs={"filename": "06_processor_test_absolute.gcode"},
        processors=RelToAbs
    )

    test( # TEST 7
        fail_type=None,
        filename="test_data/sedu.gcode",
        output=GCodeOutput,
        outargs={"filename": "07_sedu_absolute.gcode"},
        processors=RelToAbs
    )

    test( # TEST 8
        fail_type=None,
        filename="test_data/linear_test.gcode",
        output=GCodeOutput,
        outargs={"filename": "08_linear_test_processed.gcode"},
        processors=LinearJoiner
    )

    test( # TEST 9
        fail_type=None,
        filename="test_data/sedu_s.gcode",
        output=GCodeOutput,
        outargs={"filename": "09_sedu_s_normalized.gcode"},
        processors=NormalizeUnits
    )

    test( # TEST 10
        fail_type=None,
        filename="test_data/sedu_s.gcode",
        output=MotomanArcOutput,
        outargs={"job_name": "10_sedus", "user_frame": 50, "tool": 0, "arc_file": 995},
        processors=NormalizeUnits
    )

    test( # TEST 11
        fail_type=None,
        filename="test_data/sedu.gcode",
        output=MotomanArcOutput,
        outargs={"job_name": "11_sedu", "user_frame": 50, "tool": 0, "arc_file": 995}
    )

    test( # TEST 12
        fail_type=None,
        filename="test_data/sedu_no_fill.gcode",
        output=MotomanArcOutput,
        outargs={"job_name": "12_sedup", "user_frame": 50, "tool": 0, "arc_file": 995},
        processors=LinearJoiner
    )

    test( # TEST 13
        fail_type=None,
        filename="test_data/arc_test.gcode",
        output=MotomanOutput,
        outargs={"job_name": "13_arcmove"}
    )

    test( # TEST 14
        fail_type=None,
        filename="test_data/arc_test.gcode",
        output=MotomanArcOutput,
        outargs={"job_name": "14_arcweld"}
    )

    test( # TEST 15
        fail_type=None,
        filename="test_data/parser_test.gcode",
        output=GCodeOutput,
        outargs={"filename": "15_parser_test_ln.gcode", "line_numbers": "true"}
    )

    test( # TEST 16
        fail_type=None,
        filename="test_data/parser_test.gcode",
        output=GCodeOutput,
        outargs={"filename": "16_parser_test_chk.gcode", "line_numbers": "false", "checksums": True}
    )

    test( # TEST 17
        fail_type=None,
        filename="test_data/parser_test.gcode",
        output=GCodeOutput,
        outargs={"filename": "17_parser_test_ln_chk.gcode", "line_numbers": True, "checksums": True}
    )

    test( # TEST 18
        fail_type=None,
        filename="test_data/arc_test.gcode",
        output=RAPIDOutput,
        outargs={"module_name": "18_arcmove", "wobj": "table", "tool": "pen"}
    )

    test( # TEST 19
        fail_type=None,
        filename="test_data/sedu_s.gcode",
        output=RAPIDOutput,
        outargs={"module_name": "19_sedus", "wobj": "table", "tool": "pen"}
    )
# </TESTS>


if __name__ == "__main__":
    logging.basicConfig(format="%(message)s")
    log = logging.getLogger("robogcode")
    log.setLevel(logging.DEBUG)

    TEST_OUTPUT = get_path()

    if len(sys.argv) > 1 and sys.argv[1].lower() == "clean":
        clean()
    else:
        create_dirs()
        #test_parser()
        tests()
        run_tests(sys.argv[1:])
