# Gcode transpiler

Usable, but missing features

Also has bugs

### Output formats
- gcode
- ABB RAPID, movements only
    - IRC5 or similar controllers
- Yaskawa Inform, movements only
    - DX-100/200, XRC or similar controllers
- Yaskawa Inform, arc welding
    - DX-100/200, XRC or similar controllers

### Gcode features

Does *not* handle gcode with:

- parameters (#123)
- mathematical expressions
- program numbers (O123)
- tape start/end (%)

Also can't handle gcode modality

### Install

Supports setuptools, can be installed with `pip`
