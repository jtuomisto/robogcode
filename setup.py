from setuptools import setup, find_packages

setup(
    name="robogcode",
    version="0.1",
    description="Gcode transpiler",
    keywords="gcode robot",
    author="Jyri Tuomisto",
    author_email="jyri.tuomisto@pm.me",
    license="Unlicense",
    python_requires="~=3.7",
    packages=find_packages(exclude=["dist", "robogcode.egg-info", "test_data", "test_result"]),
    include_package_data=True,
    exclude_package_data={'': ['README.md']},
    entry_points={"console_scripts": ["robogcode=robogcode.main:main"]},
    url="https://gitlab.com/jtuomisto/robogcode"
)
