N0  G28 X*75
N1  M999 P"abcd efgh" S"ijkl""mnop"*56
N3  G0 X10 Y10 Z5 *100; Comment at the end of a *line*
N4  G1 F480*86
N05 G1 X10.123 (comment *inside* brackets) Y10.456 E0.032*4
N08 G91 *9
N10 G1 X-1.23    Y-2 E1.02 *76;Extra space
N21 G28 X Y*33
N22 G29.1 X1 Y1 Z1 *87; command with a period
N23 M110 N0*76
N1  M02*48
