G90 G20 ; inches
G0 X0.098 Y0.197 Z0.394
G1 X0.788 F3.937
G91 ; relative
G1 X0.788
G28 X
G21 ; millimeters
G1 X20
G28
G1 X10 Y10 Z10
G90 G93 ; absolute, inverse feed rate
G1 X20 Y20 Z20 F10
G1 X0 Y0 Z0 F5
M2
