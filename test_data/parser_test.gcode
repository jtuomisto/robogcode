G28 X
M999 P"abcd efgh" S"ijkl""mnop"
G0 X10 Y10 Z5 ; Comment at the end of a *line*
G1 F480
G1 X10.123 (comment *inside* brackets) Y10.456 E0.032
G91
G1 X-1.23    Y-2 E1.02 ;Extra space
M110 N20
G28 X Y
G29.1 X1 Y1 Z1 ; command with a period
/G28 ; skip
M02
